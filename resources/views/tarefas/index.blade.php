@extends('layouts.default')

@section('title')
Lista de Tarefas
@stop

{{-- Comentários --}}

@section('content')
<ul>
@foreach ($tarefas as $tarefa)
  <li>
      Id: {{ $tarefa->id }}
      <br/>
      Título: {{ $tarefa->titulo }}
      <br/>
      Corpo: {{ $tarefa->corpo }}
  </li>
@endforeach
</ul>
<a href="">Cadastro</a>
@stop