@extends('layouts.default')

@section('title')
Criar Tarefa
@stop

@section('content')

{{-- Documentação: http://laravelcollective.com/docs/5.1/html --}}
<div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
          <h2>Criar tarefa</h2>

    {!! Form::open(array(
       'url' => 'foo/bar',
       'class' => 'form',
       'id' => 'formulario'
    )) !!}

    <div class="form-group">
        <label for="email">E-mail:</label>
        {!! Form::text('email', 'example@gmail.com', array(
            'id'      => 'email',
            'placeholder' => 'Coloque o seu endereço de e-mail aqui...',
            'class' => 'form-control'
        )) !!}
    </div>
    
    <div class="form-group">
        <label for="password">Password:</label>
        {!! Form::password('password', array(
            'id'     => 'password',
            'placeholder' => 'Coloque a sua senha aqui...',
            'class' => 'form-control'
        )) !!}
    </div>

    <div class="form-group">
        <label for="descricao">Descrição:</label>
        {!! Form::textarea('descricao', null, array(
            'id'      => 'descricao',
            'rows'    => 5,
            'cols'    => 10,
            'placeholder' => 'Coloque a descrição aqui...',
            'class' => 'form-control',
            'data-tinymce' => 'true'
        )) !!}
    </div>
    
    <div class="form-group">
        <label for="arquivo">Arquivo:</label>
        {!! Form::file('file', array(
            'id'     => 'arquivo'
        )) !!}
    </div>
    
    <div class="form-group">
        <label for="opcoes1">Opção 1</label>
        {!! Form::checkbox('opcoes[]', 'opção1', false, array(
            'id'      => 'opcoes1',
            'class' => 'opcoes'       
        )) !!}
    </div>
    
    <div class="form-group">
        <label for="radio1">Rádio 1</label>
        {!! Form::radio('radios[]', 'radio1', false, array(
            'id'      => 'radio1',
            'class' => 'radioItem'       
        )) !!}
    </div>
    
    <div class="form-group">
        <label for="combo">Combo</label>
        {!! Form::select('combo', array(
            'Cats' => array('leopard' => 'Leopard'),
            'Dogs' => array('spaniel' => 'Spaniel'),
            ), array(
                'id'      => 'combo',
                'class' => 'form-control'       
        )) !!}
    </div>

    {!! Form::close() !!}
      </div>
    </div>
</div>
@stop